package negocio;

import org.junit.Test;


public class UnionFindTest {

	@Test(expected =IllegalArgumentException.class)
	public void verticeNegTest() {
		UnionFind u=new UnionFind(-1);
	}
	@Test(expected =IllegalArgumentException.class)
	public void verticeDeRootNegativoTest() {
		UnionFind u=new UnionFind(5);
		u.root(-1);
	}
	@Test(expected =IllegalArgumentException.class)
	public void verticeDeRootNoExisteTest() {
		UnionFind u=new UnionFind(5);
		u.root(5);
	}
	@Test(expected =IllegalArgumentException.class)
	public void iDeFindNegativoTest() {
		UnionFind u=new UnionFind(5);
		u.find(-1, 2);
	}
	@Test(expected =IllegalArgumentException.class)
	public void jDeFindNegativoTest() {
		UnionFind u=new UnionFind(5);
		u.find(1, -2);
	}
	@Test(expected =IllegalArgumentException.class)
	public void jDeFindSeExcedeTest() {
		UnionFind u=new UnionFind(5);
		u.find(1, 6);
	}
	@Test(expected =IllegalArgumentException.class)
	public void iDeFindSeExcedeTest() {
		UnionFind u=new UnionFind(5);
		u.find(8, 2);
	}
	@Test(expected =IllegalArgumentException.class)
	public void iDeUnionSeExcedeTest() {
		UnionFind u=new UnionFind(5);
		u.find(8, 2);
	}
	@Test(expected =IllegalArgumentException.class)
	public void jDeUnionSeExcedeTest() {
		UnionFind u=new UnionFind(5);
		u.find(2, 8);
	}
	@Test(expected =IllegalArgumentException.class)
	public void iDeUnionNegativoTest() {
		UnionFind u=new UnionFind(5);
		u.find(-2, 3);
	}
	@Test(expected =IllegalArgumentException.class)
	public void jDeUnionNegativoTest() {
		UnionFind u=new UnionFind(5);
		u.find(4, -2);
	}
	@Test(expected =IllegalArgumentException.class)
	public void findLoopTest() {
		UnionFind u=new UnionFind(5);
		u.find(2, 2);
	}
	@Test(expected =IllegalArgumentException.class)
	public void unionLoopTest() {
		UnionFind u=new UnionFind(5);
		u.find(4, 4);
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
