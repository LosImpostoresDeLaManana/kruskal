package negocio;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class BFSTest {

	@Test
	void alcanzablesTest() {
		
		Grafo grafo = inicializarGrafoNoConexo();
	
		assertFalse(BFS.alcanzables(grafo, 1 ).contains(0));
	}
	
	@Test
	void agregarVecinosPendientesTest() {
		Grafo grafo = inicializarGrafoNoConexo();
		BFS.inicializar(grafo, 1);
		BFS.agregarVecinosPendientes(grafo, 0);
		
		assertFalse(BFS.devolverPendientes().contains(0));
		
	}

	private Grafo inicializarGrafoNoConexo() {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(new Arista(1,3,4));
		grafo.agregarArista(new Arista(1,2,5));
		grafo.agregarArista(new Arista(4,3,1));
		return grafo;
	}

}
