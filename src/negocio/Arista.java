package negocio;

public class Arista {
	private int i,j,peso;
	
	Arista(int i,int j,int peso){
		if(i==j || i<0 || j<0) {
			throw new IllegalArgumentException("Los vertices deben ser >0: "+i+"-"+j);
		}
		if(peso<0) {
			throw new IllegalArgumentException("El peso debe ser >0: "+peso);
		}
		this.peso=peso;
		this.i=i;
		this.j=j;
	}
	
	@Override
	public String toString() {
		return "Arista [i=" + i + ", j=" + j + ", peso=" + peso + "]\n";
	}

	
	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}

	public int getJ() {
		return j;
	}

	public void setJ(int j) {
		this.j = j;
	}

	public int getPeso() {
		return peso;
	}
	
}
