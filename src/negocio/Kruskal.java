package negocio;

import java.util.ArrayList;

public class Kruskal {
	private static Grafo n;
	private static ArrayList<Arista> aristas;

	static Grafo dameAGMconUF(Grafo g) {
		if (g == null) {
			throw new RuntimeException("el grafo es nulo");
		} else {

			iniciar(g);
			UnionFind uf = new UnionFind(g.cantVertices());
			int aris = cantArista(g);
			int i = 0;
			while (i < aris) {
				Arista a = g.dameAristaPesoMinimo(n, aristas);
				if (!estaEnDiferenteCompConexa(uf, a)) {
					n.agregarArista(a);
					uf.union(a.getI(), a.getJ());
				}
				i++;
			}
			return n;
		}
	}

	private static boolean estaEnDiferenteCompConexa(UnionFind uf, Arista a) {
		return uf.find(a.getI(), a.getJ());
	}

	private static int cantArista(Grafo g) {
		return g.devolverAristasConPeso().size();
	}

	private static void iniciar(Grafo g) {
		n = new Grafo(g.cantVertices());
		aristas = clonar(g.devolverAristasConPeso());
	}

	public static Grafo dameAGMconBFS(Grafo g) {
		iniciar(g);
		int aris = cantArista(g);
		int i = 0;
		while (i < aris) {
			Arista a = g.dameAristaPesoMinimo(n, aristas);
			if (!formaCircuito(a)) {
				n.agregarArista(a);
			}
			i++;
		}
		return n;
	}

	private static ArrayList<Arista> clonar(ArrayList<Arista> aristas) {
		ArrayList<Arista> nuevo = new ArrayList<Arista>();
		for (Arista a : aristas) {
			nuevo.add(a);
		}
		return nuevo;
	}

	private static boolean formaCircuito(Arista a) {
		return BFS.alcanzables(n, a.getI()).contains(a.getJ());
	}

}
