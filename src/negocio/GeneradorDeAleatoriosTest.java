package negocio;

import org.junit.Test;

public class GeneradorDeAleatoriosTest {

	@Test(expected = IllegalArgumentException.class)
	public void grafoAleatorioNegativoTest() {
		GeneradorDeAleatorios.grafoAleatorio(-1);
	}

}
