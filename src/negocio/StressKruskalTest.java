package negocio;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class StressKruskalTest {
	static long TInicio, TFin, tiempo,inicioTotal,finTotal;
	static int cantGrafos;
	static Grafo g;
	 static JPanel panel;
	
	public static void main(String[] args) {
		cantGrafos = 100;
		 panel=new JPanel();
		 
		 DefaultCategoryDataset dataset = new DefaultCategoryDataset(); 
		 inicioTotal = System.currentTimeMillis();
		for (int i = 0; i < cantGrafos; i++) {
			g = GeneradorDeAleatorios.grafoAleatorio(i);

			// System.out.println(g.toString());
			System.out.println("Cantidad de Vertices " + g.cantVertices());
			System.out.println("Cantidad de aristas " + g.devolverAristasConPeso().size());
			
			TInicio = System.currentTimeMillis();
			Kruskal.dameAGMconBFS(g);
			
			// System.out.println("kruskal con BFS \n" + Kruskal.dameAGMconUF(g));
			TFin = System.currentTimeMillis();
			
			System.out.println("Tiempo en milis BFS: " + (TFin - TInicio) + "\n");
			dataset.addValue( (TFin - TInicio), "BFS", g.cantVertices()+"");
			TInicio = System.currentTimeMillis();
			Kruskal.dameAGMconUF(g);

			// System.out.println("kruskal con UF \n" + Kruskal.dameAGMconUF(g));
			TFin = System.currentTimeMillis();
			System.out.println("Tiempo en milis UF: " + (TFin - TInicio) + "\n");
			dataset.addValue( (TFin - TInicio), "UF", g.cantVertices()+"");
			
		}
		
		 finTotal = System.currentTimeMillis();
		 System.out.println("tiempo total :"+(finTotal-inicioTotal));
		JFreeChart chart=ChartFactory.createLineChart("Tiempo promedio de ejecucion",
                "Cantidad de Vertices","Tiempo",dataset,PlotOrientation.VERTICAL, 
                true,true,false); 
		
        
        // Mostrar Grafico
        ChartPanel chartPanel = new ChartPanel(chart);
        
        panel.add(chartPanel);
        
        ChartFrame frame = new ChartFrame("Los Impostores de la mañana", chart);
        frame.pack();
        frame.setVisible(true);

	}

}
