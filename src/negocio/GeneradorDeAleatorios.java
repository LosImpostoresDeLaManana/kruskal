package negocio;

import java.util.Random;

public class GeneradorDeAleatorios {

	public static Grafo grafoAleatorio(int cantidad) {
		if (cantidad < 0) {
			throw new IllegalArgumentException("el tamaño del grafo no puede ser negativo");
		} else {
			Grafo g = new Grafo(cantidad);

			for (int i = 0; i < cantidad; i++) {

				int peso = pesoAleatorio(cantidad);

				for (int j = 0; j < cantidad; j++) {
					if (i != j && !g.existeArista(j, i)) {
						g.agregarArista(i, j, peso);
					}
				}
			}
			return g;
		}
	}

	private static int pesoAleatorio(int cantidad) {
		Random rPeso = new Random();
		return rPeso.nextInt(cantidad * 2);
	}
}
