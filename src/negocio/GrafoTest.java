package negocio;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class GrafoTest {


	@Test(expected =IllegalArgumentException.class)
	public void primerVerticeNegativoTest() {
		Grafo g= new Grafo(5);
		g.agregarArista(-1, 3,5);
	}
	
	@Test(expected =IllegalArgumentException.class)
	public void pesoNegativoTest() {
		Grafo g= new Grafo(5);
		g.agregarArista(1, 3,-4);
	}
	
	@Test(expected =IllegalArgumentException.class)
	public void primerVerticeExcedidoTest() {
		Grafo g= new Grafo(5);
		g.agregarArista(5, 1,44);
	}
	
	@Test(expected =IllegalArgumentException.class)//estoy esperando esta exception
	public void segundoVerticeNegativoTest() {
		Grafo g= new Grafo(5);
		g.agregarArista(1, -3,4);
	}
	
	@Test(expected =IllegalArgumentException.class)//estoy esperando esta exception
	public void segundoVerticeExcedidoTest() {
		Grafo g= new Grafo(5);
		g.agregarArista(1, 5,6);
	}
	
	@Test(expected =IllegalArgumentException.class)
	public void loopsTest() {
		Grafo g= new Grafo(5);
		g.agregarArista(2, 2,6);
	}
	
	@Test(expected =IllegalArgumentException.class)
	public void agregarAristaConDistintoPesoTest() {
		Grafo grafo=new Grafo(5);
		grafo.agregarArista(2, 3,6);
		grafo.agregarArista(2, 3,5);
	}
	@Test(expected =IllegalArgumentException.class)
	public void agregarAristaConDistintoPesoInvertidaTest() {
		Grafo grafo=new Grafo(5);
		grafo.agregarArista(2, 3,5);
		grafo.agregarArista(3, 2,6);
	}
	@Test(expected =IllegalArgumentException.class)
	public void agregarYaExistenteEnListaDePeso() {
		Grafo g=new Grafo(5);
		g.agregarArista(1, 2, 5);
		g.agregarArista(4, 3, 4);
		Arista a=new Arista(1, 2, 5);
		
		g.agregarArista(a);
		
	}
	@Test(expected =IllegalArgumentException.class)
	public void agregarYaExistenteEnListaDePesoInvertida() {
		Grafo g=new Grafo(5);
		g.agregarArista(1, 2, 5);
		g.agregarArista(4, 3, 4);
		Arista ab=new Arista(2, 1, 5);
		
		g.agregarArista(ab);
		
	}
	@Test(expected =IllegalArgumentException.class)
	public void agregarYaExistenteEnListaDePesoPesoDistinto() {
		Grafo g=new Grafo(5);
		g.agregarArista(1, 2, 5);
		g.agregarArista(4, 3, 4);
		Arista a=new Arista(1, 2, 6);
		
		g.agregarArista(a);
		
	}
	@Test(expected =IllegalArgumentException.class)
	public void agregarYaExistenteEnListaDePesoPesoDistintoInvertida() {
		Grafo g=new Grafo(5);
		g.agregarArista(1, 2, 5);
		g.agregarArista(4, 3, 4);
		Arista a=new Arista(2, 1, 5);
		g.agregarArista(a);
		
	}
	@Test
	public void eliminarAristaExistenteTest() {
		Grafo grafo=new Grafo(5);
		grafo.agregarArista(2, 3,6);
		grafo.agregarArista(2, 4,6);
		
		grafo.EliminarArista(2, 4);
		assertFalse(grafo.existeArista(1, 4));
		
	}
	@Test
	public void eliminarAristaDosVecesExistenteTest() {
		Grafo grafo=new Grafo(5);
		grafo.agregarArista(2, 3,6);
		grafo.agregarArista(2, 4,6);
		
		grafo.EliminarArista(2,4);
		grafo.EliminarArista(2,4);
		assertFalse(grafo.existeArista(2, 4));	
	}

	@Test
	public void eliminarAristaNoExistenteTest() {
		Grafo grafo=new Grafo(5);
		grafo.agregarArista(2, 3,6);
		grafo.agregarArista(2, 4,6);
		
		grafo.EliminarArista(2, 3);
		assertFalse(grafo.existeArista(2, 3));
		
	}
	@Test
	public void aristeExistenteTest() {
		Grafo grafo=new Grafo(5);
		grafo.agregarArista(2, 3,6);
		grafo.agregarArista(2, 4,6);
		assertTrue(grafo.existeArista(2, 3));
		
	}

	@Test
	public void aristeOpuestaExistenteTest() {
		Grafo grafo=new Grafo(5);
		grafo.agregarArista(2, 3,6);
		assertTrue(grafo.existeArista(3,2));	
	}
	@Test
	public void noExisteAristaTest(){
		Grafo g=new Grafo(5);
		g.agregarArista(2, 3,6);
		g.agregarArista(2, 4,5);
		assertFalse(g.existeArista(1, 4));
	}
	
	
	@Test
	public void dameAristaPesoMinimo() {
		Grafo g=new Grafo(5);
		g.agregarArista(1, 2, 5);
		g.agregarArista(4, 3, 4);
		g.agregarArista(3, 1, 3);
		g.agregarArista(2, 4, 2);
		Grafo k=new Grafo(g.cantVertices());
		Arista ar=g.dameAristaPesoMinimo(k,g.devolverAristasConPeso());
		assertEquals(2, ar.getPeso());
	}
	@Test
	public void dameAristaPesoMinimoConPesosIguales() {
		Grafo g=new Grafo(5);
		g.agregarArista(1, 2, 2);
		g.agregarArista(4, 3, 2);
		g.agregarArista(3, 1, 2);
		g.agregarArista(2, 4, 2);
		Grafo k=new Grafo(g.cantVertices());

		
		Arista ar=g.dameAristaPesoMinimo(k,g.devolverAristasConPeso());
		assertEquals(2, ar.getPeso());
	}
	@Test
	public void dameAristaPesoMinimoConAlgunosValoresIngresados() {
		Grafo g=new Grafo(5);
		g.agregarArista(1, 2, 2);
		g.agregarArista(4, 3, 3);
		g.agregarArista(3, 1, 4);
		g.agregarArista(2, 4, 5);
		
		Grafo k=new Grafo(g.cantVertices());
		ArrayList<Arista> a =g.devolverAristasConPeso();
		Arista ar=g.dameAristaPesoMinimo(k,a);
		k.agregarArista(ar);
		ar=g.dameAristaPesoMinimo(k,a);
		k.agregarArista(ar);

		System.out.print(ar.getPeso());
		assertEquals(3, ar.getPeso());
	}
	
	@Test
	public void cantVertices() {
		Grafo g=new Grafo(5);
		assertEquals(5, g.cantVertices());
	}
	

}
