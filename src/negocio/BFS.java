package negocio;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


public class BFS {
	
	private static boolean [] visitados;
	private static ArrayList<Integer>pendientes;

	public static Set<Integer> alcanzables(Grafo g, int origen) {
		Set<Integer> ret=new HashSet<Integer>();
		inicializar(g,origen);
		while(pendientes.size()>0) {
			int i=pendientes.get(0);
			visitados[i]=true;
			ret.add(i);
		agregarVecinosPendientes(g,i);
		pendientes.remove(0);
		}
		return ret;
	}
	public static ArrayList<Integer> devolverPendientes(){
		return pendientes;
	}
	
	static void inicializar(Grafo g, int origen) {
	
		pendientes=new ArrayList<Integer>();
		pendientes.add(origen);
		visitados= new boolean[g.cantVertices()];
		
	}

	static void agregarVecinosPendientes(Grafo grafo, int vertice) {
		for (int v: grafo.vecinos(vertice)) {
			if(noEsVecino(v)) {
				pendientes.add(v); 
			}
		}
	}
	
	static  boolean noEsVecino(int vertice) { 
		return visitados[vertice]==false  && pendientes.contains(vertice)==false;
	}

	
	

}
