package negocio;

public class UnionFind {
	private int [] raices;
	
	public UnionFind(int vertices) {
		if(vertices<0) {
			throw new IllegalArgumentException("la cantidad de vertices no puede ser menor a 0");
		}else {
		raices = new int [vertices];
		 cargarRaices(vertices);}
	}
	
	public void cargarRaices(int vertices) {
		for(int i=0;i<vertices;i++) {
			raices[i]=i;
		}
	}
	
	public  int root(int i){//encontrar raiz de vertice
		if(i<0) {
			throw new IllegalArgumentException("el vertice de root no puede ser menor a 0");
		}else if(i>=raices.length) {
			throw new IllegalArgumentException("el vertice debe estar dentro del arreglo");	
		}
		else {
		while(raices[i]!=i) {
			i=raices[i];
		}
		return i;
		}
	}
	
	public  boolean find(int i,int j) {//misma componente conexa
		if(i==j) {
			throw new IllegalArgumentException("el vertice de root no puede ser menor a 0");
		}else {
		return root(i)==root(j);
		}
	}
	
	public  void union(int i,int j) {//raiz de una apunte a otra
		int ri=root(i);
		int rj=root(j);
		raices[ri]=rj;
	}
}
