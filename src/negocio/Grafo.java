package negocio;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Grafo {

	private boolean[][] a;
	private ArrayList<Arista> pesoDeAristas;

	public Grafo(int vertices) {
		a = new boolean[vertices][vertices];
		pesoDeAristas = new ArrayList<Arista>();

	}

	public ArrayList<Arista> devolverAristasConPeso() {
		return pesoDeAristas;
	}

	public boolean verticePertenece(int vertice) {
		return (a.length > vertice && a[vertice][vertice]);

	}

	public void agregarArista(int i, int j, int peso) {
		if (existeArista(i, j) || existeArista(j, i)) {
			throw new IllegalArgumentException("la arista ya existe");
		} else {

			verificarVertice(i);
			verificarVertice(j);
			verificarDistinto(i, j);
			agregarAa(i, j, peso);
		}
	}

	public void agregarArista(Arista arista) {
		
		int i = arista.getI();
		int j = arista.getJ();
		if (existeArista(i, j) || existeArista(j, i)) {
			throw new IllegalArgumentException("la arista ya existe");
		} else {
		int peso = arista.getPeso();

		verificarDistinto(i, j);
		agregarAa(i, j, peso);
		}
	}

	private void agregarAa(int i, int j, int peso) {
		a[i][j] = true;
		a[j][i] = true;
		Arista k = new Arista(i, j, peso);
		pesoDeAristas.add(k);
	}

	public boolean existeArista(int i, int j) {
		verificarVertice(i);
		verificarVertice(j);
		verificarDistinto(i, j);
		return a[i][j];
	}

	public void EliminarArista(int i, int j) {
		verificarVertice(i);
		verificarVertice(j);
		verificarDistinto(i, j);
		eliminarDeA(i, j);

	}

	private void eliminarDeA(int i, int j) {
		a[i][j] = false;
		a[j][i] = false;
	}

	public Set<Integer> vecinos(int i) {
		verificarVertice(i);
		Set<Integer> ret = new HashSet<Integer>();
		for (int j = 0; j < cantVertices(); ++j)
			if (i != j) {
				if (this.existeArista(i, j)) {
					ret.add(j);
				}
			}
		return ret;
	}

	private void verificarDistinto(int i, int j) {
		if (i == j) {
			throw new IllegalArgumentException("no se puede agregar loops" + "{" + i + "" + j + "}");
		}
	}

	protected void verificarVertice(int i) {
		if (i < 0) {
			throw new IllegalArgumentException("el parametro |i| es negativo" + i);
		}
		if (i >= a.length) {
			throw new IllegalArgumentException("el parametro |i| esta fuera de rango " + i);
		}
	}

	public int cantVertices() {
		return a.length;
	}

	public Arista dameAristaPesoMinimo(Grafo g, ArrayList<Arista> aris) {
		Arista ret = aris.get(0);
		for (int i = 0; i < aris.size(); i++) {
			if (esMenor(aris.get(i), ret) && noEsta(aris.get(i), g)) {
				ret = aris.get(i);
			}
		}
		aris.remove(ret);
		return ret;

	}

	private boolean esMenor(Arista a, Arista b) {
		return a.getPeso() <= b.getPeso();
	}

	private boolean noEsta(Arista a, Grafo g) {
		return !g.existeArista(a.getI(), a.getJ());
	}

	@Override
	public String toString() {
		return " " + pesoDeAristas + "\n";
	}

}
